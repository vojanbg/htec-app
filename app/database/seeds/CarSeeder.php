<?php

class CarSeeder extends DatabaseSeeder {
	
	public function run() 
	{
		$faker = Faker\Factory::create();

		$names = array(
			'BMW X6',
			'Renault Megane',
			'Chevrolet Spark',
			'Audi A8',
			'Lamborghini Aventador',
			'Fiat Bravo'
		);

		$i = 0;
		$items = array();
		for($i=0;$i<15;$i++) {

			$items[$i] = array(
				'user_id' => rand(1,2),
				'name' => $names[rand(0, count($names) - 1)],
				'description' => $faker->sentence(),
				'cc' => $faker->numberBetween(1000, 4000),
				'km' => $faker->numberBetween(20000, 200000),
				'image' => null
			);

		}
	  
	    foreach ($items as $item) {
	      $car = Car::create($item);

	      $count = rand(1,4);

	      for($i=1;$i<=$count;$i++) {
	      	$car->categories()->attach($i);
	      }

	      $car->save();
	      
	    }
	}
	
}
