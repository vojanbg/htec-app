<?php

class CategorySeeder extends DatabaseSeeder {
	
	public function run() 
	{
		$categories = array(
			array(
				"name" => "Automobili",
				"description" => "Automobili",
			),

			array(
				"name" => "Motori",
				"description" => "Motori",
			),

			array(
				"name" => "Transportna vozila",
				"description" => "Transportna vozila",
			),

			array(
				"name" => "Mehanizacija",
				"description" => "Mehanizacija",
			),
		);
	  
	    foreach ($categories as $cat) {
	      	$c = Category::create($cat);
	    }
	}
	
}
