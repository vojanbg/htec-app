<?php

class UserSeeder extends DatabaseSeeder {
	
	public function run() 
	{
		$users = array(
			array(
				"name" => "Vojislav Janjić",
				"username" => "vojislav.janjic",
	        	"password" => Hash::make("test123"),
	        	"api_key" => Hash::make('vojajanjic@gmail.com')
			),

			array(
				"name" => "Pera Perić",
				"username" => "pera.peric",
	        	"password" => Hash::make("test123"),
	        	"api_key" => Hash::make('pera.peric@gmail.com')
			),
		);
	  
	    foreach ($users as $user) {
	      User::create($user);
	    }
	}
	
}
