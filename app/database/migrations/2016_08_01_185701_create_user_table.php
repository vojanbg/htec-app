<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user', function($table) {
        			
			$table->increments('user_id');
	  		$table->string('name');
	  		$table->string('username');
			$table->string('password');
			$table->string('remember_token');
	  		$table->timestamps();

			$table->string('api_key'); 		
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user');
	}

}
