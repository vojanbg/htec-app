<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('car', function($table) {
        			
			$table->increments('car_id');
			$table->integer('user_id')->unsigned();
	  		$table->string('name');
	  		$table->text('description');
	  		$table->integer('cc');
	  		$table->integer('km');
	  		$table->string('image')->nullable();
	  		$table->timestamps();

	  		$table->foreign('user_id')->references('user_id')->on('user'); 
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('car');
	}

}
