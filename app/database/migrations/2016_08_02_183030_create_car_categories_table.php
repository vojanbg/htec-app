<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarCategoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('car_categories', function($table) {
        			
			$table->integer('car_id')->unsigned();
			$table->integer('category_id')->unsigned();
			
			$table->primary(array('car_id', 'category_id'));			
			$table->foreign('car_id')->references('car_id')->on('car'); 
			$table->foreign('category_id')->references('category_id')->on('category');  
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('car_categories');
	}

}
