<?php

function api_response($response = array(), $code = 400, $data = array()) {
    $result['response'] = $response;
    $result['response']['code'] = $code;

    if (is_array($data) && count($data) > 0) {
        $result['data'] = $data;
    }

    return Response::json($result, $code);
}

function generate_random_string($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function check_base64_image($base64) {
    $img = @imagecreatefromstring(base64_decode($base64));
    if (!$img) {
        return false;
    }

    $destination_path = Config::get('constants.image_path');
    $session_id = Session::getId();
    $filename = $destination_path . '/' . $session_id . '.png';

    imagepng($img, $filename);
    $info = getimagesize($filename);
    unlink($filename);

    if ($info[0] > 0 && $info[1] > 0 && $info['mime']) {
        return true;
    }

    return false;
}

function save_base64_image($base64_image) {
	$raw_image = base64_decode($base64_image);

	// Create image folders if needed
    if (!is_dir(Config::get('constants.image_path') . '/' . date('Y'))) {
        mkdir(Config::get('constants.image_path') . '/' . date('Y'));
    }

    if (!is_dir(Config::get('constants.image_path') . '/' . date('Y') . '/' . date('m'))) {
        mkdir(Config::get('constants.image_path') . '/' . date('Y') . '/' . date('m'));
    }

    if (!is_dir(Config::get('constants.image_path') . '/' . date('Y') . '/' . date('m') . '/' . date('d'))) {
        mkdir(Config::get('constants.image_path') . '/' . date('Y') . '/' . date('m') . '/' . date('d'));
    }

    $destination_path = Config::get('constants.image_path') . '/' . date('Y') . '/' . date('m') . '/' . date('d');

    $filename = generate_random_string(7) . '-' . time() . '.png';

    // Save image to a file
    $fp = @fopen($destination_path . '/' . $filename, 'w+');
    @fwrite($fp, $raw_image);
    @fclose($fp);

    // File not writable
    if (!file_exists($destination_path . '/' . $filename)) {
        return false;
    }

    return $destination_path . '/' . $filename;
}