<?php

namespace Api;
use ApiController;
use DB;
use Auth;
use Input;
use Validator;

use Car;
use User;

class CarController extends ApiController {

	public function __construct() 
	{		
		parent::__construct();
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$items = Car::orderBy('name','asc')
					->with('categories')
					->get();

		$data['items'] = $items->toArray();
		return api_response(array(), 200, $data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		if(!ctype_digit($id) || !Car::find($id)) {
			$response['message'] = "Car ID not valid.";
			$response['success'] = false;		
			return api_response($response, 400);
		}

		$item = Car::where('car_id', '=', $id)
					->with('categories')
					->first();

		$data['item'] = $item;
		return api_response(array(), 200, $data);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		if(!ctype_digit($id) || !$item = Car::find($id)) {
			$response['message'] = "Car ID not valid.";
			$response['success'] = false;		
			return api_response($response, 400);
		}

		$api_key = Input::get('api_key');
		$user = User::where('api_key','=',$api_key)->first();

		if($item->user_id != $user->user_id) {
			$response['message'] = "You can update only your own cars.";
			$response['success'] = false;		
			return api_response($response, 400);
		}

		Validator::extend('base64_image', function($attribute, $value)
        {
        	if(!check_base64_image($value)) {
        		return false;
        	}

            return true;
        });

		$rules = array(
    		'name' => 'max:255',
    		'cc' => 'integer',
    		'km' => 'integer',
    		'image' => 'base64_image'
    	);
		
		$messages = array(
			'name.max' => 'Car name cannot be longer than 255 characters',
			'cc.integer' => 'Cc must be an integer',
			'km.integer' => 'Km must be an integer',
			'image.base64_image' => 'Image not valid'
		);
		
		$validator = Validator::make(Input::all(), $rules, $messages);

		if($validator->passes())  {
			DB::beginTransaction();

			if(Input::has('name')) {
				$item->name = Input::get('name');	
			}

			if(Input::has('description')) {
				$item->description = Input::get('description');	
			}

			if(Input::has('cc')) {
				$item->cc = Input::get('cc');	
			}

			if(Input::has('km')) {
				$item->km = Input::get('km');	
			}

			if(Input::has('image')) {
				$path = save_base64_image(Input::get('image'));
				$item->image = $path;
			}

			if(!$item->save()) {
				DB::rollback();

				$response['message'] ="An error occurred while saving to the database. Please try again later.";
				$response['success'] = false;
				return api_response($response, 400);
			}

			DB::commit();

			$response['message'] ="Car updated successfully.";
			$response['success'] = true;
			return api_response($response, 200);
		} else {
			$response['message'] = $validator->messages()->first();
			$response['success'] = false;
			return api_response($response, 400);
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		if(!ctype_digit($id) || !$item = Car::find($id)) {
			$response['message'] = "The car you are trying to delete does not exist.";
			$response['success'] = false;		
			return api_response($response, 400);
		}

		$api_key = Input::get('api_key');
		$user = User::where('api_key','=',$api_key)->first();

		if($item->user_id != $user->user_id) {
			$response['message'] = "You can delete only your own cars.";
			$response['success'] = false;		
			return api_response($response, 400);
		}

		DB::beginTransaction();

		// Remove connections between car and categories
		$item->categories()->detach();

		if(!$item->delete($id)) {
			DB::rollback();

			$response['message'] ="An error occurred while deleting the car from the database. Please try again later.";
			$response['success'] = false;
			return api_response($response, 400);
		}

		DB::commit();

		$response['message'] = "Car successfully deleted.";
		$response['success'] = true;
		return api_response($response, 200);
	}


}
