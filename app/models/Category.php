<?php

class Category extends Eloquent {
	
	protected $table = 'category';

	protected $primaryKey = 'category_id';

	public $timestamps = false;

	public function cars() {
		return $this->belongsToMany('Car','car_categories');
	}
}
