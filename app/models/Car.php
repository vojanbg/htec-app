<?php

class Car extends Eloquent {
	
	protected $table = 'car';

	protected $primaryKey = 'car_id';

	public $timestamps = true;

	public function user()
	{
		return $this->belongsTo('User');
	}

	public function categories()
	{
		return $this->belongsToMany('Category','car_categories');
	}
}
