<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('admin/login', 'App\UserController@getLogin');
Route::post('admin/login', 'App\UserController@postLogin');
Route::get('admin/logout', 'App\UserController@getLogout');
Route::get('admin', 'App\UserController@getLogin');
Route::post('admin', 'App\UserController@postLogin'); 

/* API routes */ 
Route::group(array('prefix' => 'api'), function()
{
    Route::resource('car', 'Api\CarController');
});